{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Evapotranspiration timeseries plots\n",
    "This Jupyter Notebook is used to make daily and monthly climatology plots of observed and simulated evapotranspiration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pathlib as pl\n",
    "\n",
    "save_directory = pl.Path(\"../../saves/common/et\")\n",
    "observation_directory = pl.Path(\"../../saves/observations/et\")\n",
    "simulation_directory = pl.Path(\"../../saves/simulations/et\")\n",
    "output_directory = pl.Path(\"../../saves/performance/et\")\n",
    "\n",
    "min_overlap = 365 * 2 # days"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions\n",
    "Functions to make timeseries plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Optional\n",
    "import pandas as pd\n",
    "import plotnine as pn\n",
    "\n",
    "def plot_timeseries(evaporation: pd.DataFrame,\n",
    "                    y_label: str,\n",
    "                    title: Optional[str] = None,) -> pn.ggplot:\n",
    "    \n",
    "    ggplt = pn.ggplot(data = evaporation, mapping = pn.aes(x = \"date\"))        \n",
    "    ggplt += pn.geom_line(mapping = pn.aes(y = y_label,\n",
    "                                           color = \"type\",\n",
    "                                           group = \"type\"))\n",
    "    ggplt += pn.scale_x_date(name=\"Date\")\n",
    "    ggplt += pn.scale_y_continuous(name=\"Discharge (m3 s-1)\")\n",
    "    ggplt += pn.scale_color_manual(name=\"\", values={\"observed\": \"black\",\n",
    "                                                    \"simulated\": \"red\"})\n",
    "    if title is not None:\n",
    "        ggplt += pn.ggtitle(title)\n",
    "    ggplt += pn.theme(panel_background=pn.element_blank(),\n",
    "                        panel_grid_major=pn.element_blank(),\n",
    "                        panel_grid_minor=pn.element_blank())\n",
    "    return ggplt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot timeseries\n",
    "Plot timeseries of observed and simulated evapotranspiration based on their common overlap period. Note that dates missing in the observations are also set to missing in the simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "import pandas as pd\n",
    "\n",
    "regions = [dir.stem for dir in simulation_directory.iterdir() if dir.is_dir()]\n",
    "\n",
    "for region in regions:\n",
    "    print(\"Region: {}\".format(region))\n",
    "    \n",
    "    region_directory = pl.Path(\"{}/{}\".format(simulation_directory, region))\n",
    "    patterns = [dir.stem for dir in region_directory.iterdir() if dir.is_dir()]\n",
    "        \n",
    "    meta_file = pl.Path(\"{}/{}/meta.parquet\".format(save_directory, region))\n",
    "    meta = pd.read_parquet(meta_file)\n",
    "    \n",
    "    for pattern in patterns:\n",
    "        print(\"\\tPattern: {}\".format(pattern))\n",
    "        \n",
    "        plot_out = pl.Path(\"{}/{}/{}_timeseries.pdf\".format(output_directory, region, pattern))\n",
    "        if plot_out.exists():\n",
    "            print(\"\\t- Already exists\")\n",
    "            continue\n",
    "        \n",
    "        ggplts = []\n",
    "          \n",
    "        for index in meta.index:\n",
    "            \n",
    "            observed_evaporation_file = pl.Path(\"{}/{}/data/evaporation_{}.parquet\".format(observation_directory, region, index))\n",
    "            observed_evaporation = pd.read_parquet(observed_evaporation_file)\n",
    "            simulated_evaporation_file = pl.Path(\"{}/{}/{}/data/evaporation_{}.parquet\".format(simulation_directory, region, pattern, index))\n",
    "            simulated_evaporation = pd.read_parquet(simulated_evaporation_file)\n",
    "            evaporation = pd.merge(observed_evaporation, simulated_evaporation, on = \"date\")\n",
    "            evaporation = evaporation.rename(columns = {\"evaporation_x\": \"observed\",\n",
    "                                          \"evaporation_y\": \"simulated\"})\n",
    "            evaporation = evaporation[[\"date\", \"observed\", \"simulated\"]]\n",
    "            \n",
    "            if evaporation.index.size == 0:\n",
    "                print(\"\\t> No evaporation in overlap period for tower {}, skipping...\".format(index))\n",
    "                continue\n",
    "            \n",
    "            if evaporation.index.size < min_overlap:\n",
    "                print(\"\\t> To few evaporation in overlap period (only {} days) for tower {}, skipping...\".format(evaporation.index.size, \n",
    "                                                                                                                  index))\n",
    "                continue\n",
    "            \n",
    "            evaporation = pd.melt(evaporation,\n",
    "                                id_vars=[\"date\"],\n",
    "                                var_name = \"type\",\n",
    "                                value_name=\"evaporation\")\n",
    "            \n",
    "            ggplt = plot_timeseries(evaporation = evaporation,\n",
    "                                    y_label = \"evaporation\",\n",
    "                                    title=\"Evapotranspiration for tower {}\".format(index))\n",
    "            ggplts.append(ggplt)\n",
    "            \n",
    "        if len(ggplts) <= 0:\n",
    "            continue\n",
    "        \n",
    "        plot_out.parent.mkdir(parents=True, exist_ok=True)\n",
    "        with warnings.catch_warnings():\n",
    "            warnings.filterwarnings(\"ignore\", module = \"plotnine\\..*\")\n",
    "            pn.save_as_pdf_pages(plots = ggplts, filename= plot_out, verbose=False)\n",
    "        \n",
    "        print(\"\\t- Plotted timeseries\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "4dHydro",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
