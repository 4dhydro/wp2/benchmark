import pathlib as pl
import pandas as pd
import plotnine as pn
import numpy as np
import geopandas as gp
import mizani as mz

performance_directories = {"et": pl.Path("../../saves/performance/et"),
                            "q": pl.Path("../../saves/performance/q"),
                            "q_alternative": pl.Path("../../saves/performance/q_alternative"),
                            "sm": pl.Path("../../saves/performance/sm"),
                            "sm_alternative": pl.Path("../../saves/performance/sm_alternative"),
                            }
additional_directory = pl.Path("../../data/additional")
output_directory = pl.Path("../../saves/performance/combined")

region_extents = {
    "europe": [-11, 33, 42, 73],
    "rhine": [3, 46, 13, 53],
    "po": [6, 43, 13, 47],
    "tugela": [28, -30, 32, -27],
}
basin_ids = {"po": 2050013010,
             "rhine": 2050023010,
             "tugela": 1050013070}
variable_names = {"et": "Evapotranspiration",
                  "q": "Discharge",
                  "q_alternative": "Discharge\n(additional)",
                  "tws": "Total water storage anomaly",
                  "tws_alternative": "Total water storage anomaly\n(additional)",
                  "sm": "Soil moisture content anomaly",
                  "sm_alternative": "Soil moisture content anomaly\n(additional)"}
region_names = {"europe": "Europe",
                  "po": "Po basin",
                  "rhine": "Rhine basin",
                  "tugela": "Tugela basin"}
metric_names = {"kge": "kge",
                "kge_r": "correlation",
                "kge_alpha": "variability ratio",
                "kge_beta": "bias ratio"}

basins_eu_file = pl.Path("{}/HydroBASINS/hybas_eu_lev05_v1c.shp".format(additional_directory))
basins_eu = gp.read_file(basins_eu_file)
basins_af_file = pl.Path("{}/HydroBASINS/hybas_af_lev05_v1c.shp".format(additional_directory))
basins_af = gp.read_file(basins_af_file)
coastlines_file = pl.Path("{}/Stanford/sg048gr3784.shp".format(additional_directory))
coastlines = gp.read_file(coastlines_file)

shapefiles = {}
for region in region_names.keys():
    print("Region: {}".format(region))
    if region not in basin_ids.keys():
      continue
    basin_id = basin_ids[region]
    if region == "tugela":
        basins_sel = np.logical_or(basins_af["NEXT_SINK"] == basin_id,
                                  basins_af["HYBAS_ID"] == basin_id)
        shapefile = basins_af.loc[basins_sel].dissolve()
    else:
        basins_sel = np.logical_or(basins_eu["NEXT_SINK"] == basin_id,
                                  basins_eu["HYBAS_ID"] == basin_id)
        shapefile = basins_eu.loc[basins_sel].dissolve()
    shapefiles[region] = shapefile

performances = []

for variable, performance_directory in performance_directories.items():
    print("Variable: {}".format(variable))
        
    regions = [dir.stem for dir in performance_directory.iterdir() if dir.is_dir()]

    for region in regions:
        print("\tRegion: {}".format(region))
        
        region_directory = pl.Path("{}/{}".format(performance_directory, region))
        patterns = [dir.stem for dir in region_directory.iterdir() if dir.is_dir()]
        
        for pattern in patterns:
            performance_file = pl.Path("{}/{}/{}/performance_deseasonalized.csv".format(performance_directory, region, pattern))
            performance = pd.read_csv(performance_file)
            
            keep_columns = ["region", "pattern", "station", "lat", "lon", "aggregation", "kge"]
            columns = [column for column in performance.columns if column in keep_columns]
            performance = performance[columns]
            
            performance["variable"] = variable
            performances.append(performance)
performance = pd.concat(performances, axis = 0)

performance = performance.drop("aggregation", axis = 1)

performance = pd.melt(performance,
                      id_vars = ["variable", "region", "pattern", "station", "lat", "lon"],
                      var_name = "metric")

performance = performance.loc[~np.isnan(performance["value"])]

metric_sel = np.logical_or(performance["variable"] == "tws",
                            performance["variable"] == "tws_alternative")
metric_sel = np.logical_and(metric_sel,
                            performance["metric"] == "kge_beta")
performance = performance.drop(performance.index[metric_sel])
metric_sel = np.logical_or(performance["variable"] == "sm",
                           performance["variable"] == "sm_alternative")
metric_sel = np.logical_and(metric_sel,
                            performance["metric"] == "kge_beta")
performance = performance.drop(performance.index[metric_sel])

performance["variable"] = performance["variable"].replace(variable_names)
performance["region"] = performance["region"].replace(region_names)

station_performance = performance.groupby(["variable", "region", "station", "lon", "lat", "metric"]).aggregate({"value": "median"})
station_performance = station_performance.dropna()
station_performance = station_performance.reset_index()

additional_sel = np.array(["(additional)" in variable for variable in performance["variable"]])
performance_base = performance.loc[~additional_sel]
performance_additional = performance.loc[additional_sel]

station_performance_bases = {}
for region, name in region_names.items():
  station_performance_bases[region] = performance_base.loc[performance_base["region"] == name]

additional_sel = np.array(["(additional)" in variable for variable in station_performance["variable"]])
station_performance_base = station_performance.loc[~additional_sel]
station_performance_additional = station_performance.loc[additional_sel]

station_performance_additionals = {}
for region, name in region_names.items():
  station_performance_additionals[region] = station_performance_additional.loc[station_performance_additional["region"] == name]
  
region, extent = list(region_extents.items())[0]
for region, extent in region_extents.items():
  station_performance_base = station_performance_bases[region]
  station_performance_additional = station_performance_additionals[region]
  
  shapefile = coastlines
  if region != 'europe':
    shapefile = shapefiles[region]
  
  if station_performance_base.index.size > 0:
    nvariables = station_performance_base["variable"].unique().size
    
    ggplt = pn.ggplot()
    ggplt += pn.geom_map(data = shapefile, mapping = pn.aes(), fill=None)
    ggplt += pn.geom_point(data = station_performance_base,
                          mapping = pn.aes(x = "lon",
                                            y = "lat",
                                            color = "value",),
                          #  position = pn.position_jitter(width = 0.1),
                            size = 2,
                            alpha = 0.5)
    ggplt += pn.scale_x_continuous(name="Longitude (degrees east)")
    ggplt += pn.scale_y_continuous(name="Latitude (degrees north)")
    ggplt += pn.coord_fixed(xlim = (extent[0],
                                    extent[2]),
                            ylim = (extent[1],
                                    extent[3]))
    ggplt += pn.scale_color_distiller(name="KGE\n",
                                      limits = (-2, 1),
                                      oob = mz.bounds.squish,
                                      type = "div",
                                      palette = "RdYlBu",
                                      direction = 1)
    ggplt += pn.facet_grid("~variable")
    ggplt += pn.ggtitle("Deseasonalized simulation performance for {}".format(region_names[region]))
    ggplt += pn.theme(panel_background=pn.element_blank(),
                      panel_grid_major=pn.element_blank(),
                      panel_grid_minor=pn.element_blank(),
                      axis_text_x=pn.element_text(rotation=45, hjust=1),
                      # legend_position="bottom",
                      # legend_direction="vertical",
                      figure_size=(3 * nvariables + 2, 3.5))
    print(ggplt)

    plot_out = pl.Path("{}/performance_base_deseasonalized_map_{}.pdf".format(output_directory,
                                                              region))
    plot_out.parent.mkdir(parents=True, exist_ok=True)
    ggplt.save(plot_out)

  if station_performance_additional.index.size > 0:
    nvariables = station_performance_additional["variable"].unique().size
    
    ggplt = pn.ggplot()
    ggplt += pn.geom_map(data = shapefile, mapping = pn.aes(), fill=None)
    ggplt += pn.geom_point(data = station_performance_additional,
                            mapping = pn.aes(x = "lon",
                                              y = "lat",
                                              color = "value",),
                            #  position = pn.position_jitter(width = 0.1),
                              size = 2,
                              alpha = 0.5)
    ggplt += pn.scale_x_continuous(name="Longitude (degrees east)")
    ggplt += pn.scale_y_continuous(name="Latitude (degrees north)")
    ggplt += pn.coord_fixed(xlim = (extent[0],
                                    extent[2]),
                            ylim = (extent[1],
                                    extent[3]))
    ggplt += pn.scale_color_distiller(name="KGE\n",
                                      limits = (-2, 1),
                                      oob = mz.bounds.squish,
                                      type = "div",
                                      palette = "RdYlBu",
                                      direction = 1)
    ggplt += pn.facet_grid("~variable")
    ggplt += pn.ggtitle("Additional deseasonalized simulation performance for {}".format(region_names[region]))
    ggplt += pn.theme(panel_background=pn.element_blank(),
                      panel_grid_major=pn.element_blank(),
                      panel_grid_minor=pn.element_blank(),
                      axis_text_x=pn.element_text(rotation=45, hjust=1),
                      # legend_position="bottom",
                      # legend_direction="vertical",
                      figure_size=(3 * nvariables + 2, 3.5))
    print(ggplt)

    plot_out = pl.Path("{}/performance_additional_deseasonalized_map_{}.pdf".format(output_directory,
                                                              region))
    plot_out.parent.mkdir(parents=True, exist_ok=True)
    ggplt.save(plot_out)