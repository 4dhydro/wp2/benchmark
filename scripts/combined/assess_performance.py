import pathlib as pl
import pandas as pd

performance_directories = {"et": pl.Path("../../saves/performance/et"),
                            "q": pl.Path("../../saves/performance/q"),
                            "q_alternative": pl.Path("../../saves/performance/q_alternative"),
                            "tws": pl.Path("../../saves/performance/tws"),
                            "tws_alternative": pl.Path("../../saves/performance/tws_alternative"),
                            "sm": pl.Path("../../saves/performance/sm"),
                            "sm_alternative": pl.Path("../../saves/performance/sm_alternative")}

performances = []

for variable, performance_directory in performance_directories.items():
    print("Variable: {}".format(variable))
        
    regions = [dir.stem for dir in performance_directory.iterdir() if dir.is_dir()]

    for region in regions:
        print("\tRegion: {}".format(region))
        
        region_directory = pl.Path("{}/{}".format(performance_directory, region))
        patterns = [dir.stem for dir in region_directory.iterdir() if dir.is_dir()]
        
        for pattern in patterns:        
            performance_file = pl.Path("{}/{}/{}/performance.csv".format(performance_directory, region, pattern))
            performance = pd.read_csv(performance_file)
            
            keep_columns = ["region", "pattern", "station", "aggregation", "kge", "kge_r", "kge_alpha", "kge_beta"]
            columns = [column for column in performance.columns if column in keep_columns]
            performance = performance[columns]
            
            performance["variable"] = variable
            performances.append(performance)
performance = pd.concat(performances, axis = 0)
performance.loc[pd.isna(performance["station"]), "station"] = "all"

aggregation_sel = (performance["variable"] == "tws") | (performance["variable"] == "tws_alternative") | ( performance["aggregation"] == "daily")
performance = performance.loc[aggregation_sel]
performance = performance.drop("aggregation", axis = 1)

performance = pd.melt(performance,
                      id_vars = ["variable", "region", "pattern", "station"],
                      var_name = "metric")

drop_sel = (performance['metric'] == 'kge_beta') & ((performance['variable'] == 'sm') | (performance['variable'] == 'tws'))
performance = performance.loc[~drop_sel]
drop_sel = (performance['metric'] == 'kge_beta') & ((performance['variable'] == 'sm_alternative') | (performance['variable'] == 'tws_alternative'))
performance = performance.loc[~drop_sel]

performance["model"] = [sim.split("_")[0] for sim in performance["pattern"]]
performance["meteo"] = [sim.split("_")[1] for sim in performance["pattern"]]
performance["res"] = [sim.split("_")[3] for sim in performance["pattern"]]
performance["meteores"] = ["with {} at {}".format(meteo, res) for meteo, res in zip(performance["meteo"], performance["res"])]

# performance.loc[performance['metric'] == 'kge_beta', 'variable'].unique()

print(performance.groupby(['metric']).aggregate({"value": "median"}))
print(performance.groupby(['region', 'metric']).aggregate({"value": "median"}))
print(performance.groupby(["variable", 'metric']).aggregate({"value": "median"}))
print(performance.groupby(['region', "variable", 'metric']).aggregate({"value": "median"}))

drop_sel = (performance['metric'] != 'kge')
performance_kge = performance.loc[~drop_sel]

print(performance_kge.groupby(['region', 'model', 'metric']).aggregate({"value": "median"}))
