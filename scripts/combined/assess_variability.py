import pathlib as pl
import numpy as np
import pandas as pd

def q25(x):
    return x.quantile(0.25)
def q75(x):
    return x.quantile(0.75)

performance_directories = {"et": pl.Path("../../saves/performance/et"),
                            "q": pl.Path("../../saves/performance/q"),
                            # "q_alternative": pl.Path("../../saves/performance/q_alternative"),
                            "tws": pl.Path("../../saves/performance/tws"),
                            # "tws_alternative": pl.Path("../../saves/performance/tws_alternative"),
                            "sm": pl.Path("../../saves/performance/sm"),
                            # "sm_alternative": pl.Path("../../saves/performance/sm_alternative")
                            }

performances = []

for variable, performance_directory in performance_directories.items():
    print("Variable: {}".format(variable))
        
    regions = [dir.stem for dir in performance_directory.iterdir() if dir.is_dir()]

    for region in regions:
        print("\tRegion: {}".format(region))
        
        region_directory = pl.Path("{}/{}".format(performance_directory, region))
        patterns = [dir.stem for dir in region_directory.iterdir() if dir.is_dir()]
        
        for pattern in patterns:        
            performance_file = pl.Path("{}/{}/{}/performance.csv".format(performance_directory, region, pattern))
            performance = pd.read_csv(performance_file)
            
            keep_columns = ["region", "pattern", "station", "aggregation", "kge", "kge_r", "kge_alpha", "kge_beta"]
            columns = [column for column in performance.columns if column in keep_columns]
            performance = performance[columns]
            
            performance["variable"] = variable
            performances.append(performance)
performance = pd.concat(performances, axis = 0)
performance.loc[pd.isna(performance["station"]), "station"] = "all"

aggregation_sel = (performance["variable"] == "tws") | (performance["variable"] == "tws_alternative") | ( performance["aggregation"] == "daily")
performance = performance.loc[aggregation_sel]
performance = performance.drop("aggregation", axis = 1)

performance = pd.melt(performance,
                      id_vars = ["variable", "region", "pattern", "station"],
                      var_name = "metric")

# performance.loc[performance['variable'] == 'q_alternative', 'variable'] = 'q'
# performance.loc[performance['variable'] == 'tws_alternative', 'variable'] = 'tws'
# performance.loc[performance['variable'] == 'sm_alternative', 'variable'] = 'sm'

drop_sel = (performance['metric'] != 'kge')
performance = performance.loc[~drop_sel]

performance["model"] = [sim.split("_")[0] for sim in performance["pattern"]]
performance["meteo"] = [sim.split("_")[1] for sim in performance["pattern"]]
performance["res"] = [sim.split("_")[3] for sim in performance["pattern"]]
performance["meteores"] = ["with {} at {}".format(meteo, res) for meteo, res in zip(performance["meteo"], performance["res"])]

variable = performance["variable"].unique()[0]
for variable in performance["variable"].unique():
    print("variable {}".format(variable))
    
    variable_performance = performance.loc[performance["variable"] == variable]
    
    grouped_performance = variable_performance.groupby('model').aggregate({"value": [q25, q75]})
    grouped_performance.columns = ['q25', 'q75']
    grouped_performance['variability'] = grouped_performance['q75'] - grouped_performance['q25']
    grouped_performance.pop('q25')
    grouped_performance.pop('q75')
    grouped_performance = grouped_performance.loc[grouped_performance['variability'] > 0]
    model_performance = grouped_performance
    
    grouped_performance = variable_performance.groupby('region').aggregate({"value": [q25, q75]})
    grouped_performance.columns = ['q25', 'q75']
    grouped_performance['variability'] = grouped_performance['q75'] - grouped_performance['q25']
    grouped_performance.pop('q25')
    grouped_performance.pop('q75')
    grouped_performance = grouped_performance.loc[grouped_performance['variability'] > 0]
    region_performance = grouped_performance
    
    print(f'model {model_performance.median().values[0]} region {region_performance.median().values[0]}')
    if model_performance.median().values[0] > region_performance.median().values[0]:
        print(model_performance)
    else:
        print(region_performance)