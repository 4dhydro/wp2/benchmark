import pathlib as pl
import pandas as pd
import numpy as np

performance_directories = {"et": pl.Path("../../saves/observations/et"),
                            "q": pl.Path("../../saves/observations/q"),
                            "q_alternative": pl.Path("../../saves/observations/q_alternative"),
                            "tws": pl.Path("../../saves/observations/tws"),
                            "tws_alternative": pl.Path("../../saves/observations/tws_alternative"),
                            "sm": pl.Path("../../saves/observations/sm"),
                            "sm_alternative": pl.Path("../../saves/observations/sm_alternative"),
                            }

min_overlaps = {"et": 365 * 3,
                "q": 365 * 3,
                "q_alternative": 365 * 2,
                "tws": 12 * 3,
                "tws_alternative": 12 * 3,
                "sm": 365 * 3,
                "sm_alternative": 365 * 3,
                }

observations = []

for variable, performance_directory in performance_directories.items():
    print("Variable: {}".format(variable))
    
    min_overlap = min_overlaps[variable]
        
    regions = [dir.stem for dir in performance_directory.iterdir() if dir.is_dir()]

    for region in regions:
        print("\tRegion: {}".format(region))
        
        region_directory = pl.Path("{}/{}".format(performance_directory, region))
        region_data_directory = pl.Path("{}/data".format(region_directory))
        
        station_files = [file for file in region_data_directory.iterdir() if file.is_file()]
        
        for station_file in station_files:        
            station_data = pd.read_parquet(station_file)
            
            if station_data.index.size == 0:
                print("\t\t> No data in observation period for station {}, skipping...".format(station_file))
                continue
            
            if station_data.index.size < min_overlap:
                print("\t\t> To few data in observation period (only {} days) for station {}, skipping...".format(station_data.index.size, 
                                                                                                                  station_file))
                continue
            
            # if "sm" in variable:
            #     print(station_data.head())
            
            observation = {"region": [region],
                           "variable": [variable],
                           "file": [station_file],
                           "duration": [station_data.index.size],
                           "group": "default"}
            
            if "group" in station_data.columns:
                for group in station_data["group"].unique():
                    group_station_data = station_data[station_data["group"] == group]
                    observation["duration"] = [group_station_data.index.size]
                    observation["group"] = [group]
                    observations.append(pd.DataFrame(observation))
            else:
                observations.append(pd.DataFrame(observation))

observation = pd.concat(observations, axis = 0)
observation = observation.groupby(["variable", "region"]).aggregate({"file": "nunique",
                                                                     "duration": "mean"})
observation = observation.reset_index()

tws_sel = np.array(["tws" in variable for variable in observation["variable"]])
observation.loc[tws_sel, "duration"] /= 12
observation.loc[~tws_sel, "duration"] /= 365

print(observation)

