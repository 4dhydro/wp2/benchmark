import pathlib as pl
import pandas as pd
import plotnine as pn
import numpy as np

performance_directories = {"et": pl.Path("../../saves/performance/et"),
                            "q": pl.Path("../../saves/performance/q"),
                            "q_alternative": pl.Path("../../saves/performance/q_alternative"),
                            "tws": pl.Path("../../saves/performance/tws"),
                            "tws_alternative": pl.Path("../../saves/performance/tws_alternative"),
                            "sm": pl.Path("../../saves/performance/sm"),
                            "sm_alternative": pl.Path("../../saves/performance/sm_alternative"),
                            }
output_directory = pl.Path("../../saves/performance/combined")

variable_names = {"et": "Evapotranspiration",
                  "q": "Discharge",
                  "q_alternative": "Discharge\n(additional)",
                  "tws": "Total water storage anomaly",
                  "tws_alternative": "Total water storage anomaly\n(additional)",
                  "sm": "Soil moisture content anomaly",
                  "sm_alternative": "Soil moisture content anomaly\n(additional)"}
region_names = {"europe": "Europe",
                  "po": "Po basin",
                  "rhine": "Rhine basin",
                  "tugela": "Tugela basin"}
model_names = {"clm": "CLM",
               "geoframe": "GEOframe",
               "mhm": "mHM",
               "parflowclm": "Parflow-CLM",
               "pcrglobwb": "PCR-GLOBWB",
               "tsmp": "TSMP",
               "tetis": "TETIS",
               "wflowsbm": "wflow_sbm",}
meteo_names = {"eobs": "E-OBS",
               "era5": "ERA-5",
               "rea6": "COMOS REA-6",
               "emo": "EMO-1",
               "hres": "HRES",
               "erai": "ERA-Interim"}
res_names = {"0p125deg": "0.125 degrees",
             "30sec": "30 arc-seconds",
             "0p0275deg": "0.275 degrees",
             "0p015625deg": "0.015625 degrees",
             "0p11deg": "0.11 degrees",
             "05min": "5 arc-minutes",
             "0p0625deg": "0.0625 degrees",}
metric_names2 = {"prime": {"kge-prime": "kge prime",
                "kge-prime_r": "correlation",
                "kge-prime_alpha": "variability ratio",
                "kge-prime_beta": "bias ratio"},
                "nonpar": {"kge-nonpar": "kge nonparametric",
                "kge-nonpar_r": "correlation",
                "kge-nonpar_alpha": "variability ratio",
                "kge-nonpar_beta": "bias ratio"}}
model_colors = dict(zip(model_names.values(), 
                        ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#ffff33','#a65628','#f781bf']))

for variant in ["prime", "nonpar"]:
  performances = []
  
  metric_names = metric_names2[variant]

  for variable, performance_directory in performance_directories.items():
      print("Variable: {}".format(variable))
          
      regions = [dir.stem for dir in performance_directory.iterdir() if dir.is_dir()]

      for region in regions:
          print("\tRegion: {}".format(region))
          
          region_directory = pl.Path("{}/{}".format(performance_directory, region))
          patterns = [dir.stem for dir in region_directory.iterdir() if dir.is_dir()]
          
          for pattern in patterns:        
              performance_file = pl.Path("{}/{}/{}/performance.csv".format(performance_directory, region, pattern))
              performance = pd.read_csv(performance_file)
              
              keep_columns = ["region", "pattern", "station", "aggregation", 
                              "kge-{}".format(variant), "kge-{}_r".format(variant), "kge-{}_alpha".format(variant), "kge-{}_beta".format(variant)]
              columns = [column for column in performance.columns if column in keep_columns]
              performance = performance[columns]
              
              performance["variable"] = variable
              performances.append(performance)
  performance = pd.concat(performances, axis = 0)
  performance.loc[pd.isna(performance["station"]), "station"] = "all"

  aggregation_sel = (performance["variable"] == "tws") | (performance["variable"] == "tws_alternative") | ( performance["aggregation"] == "daily")
  performance = performance.loc[aggregation_sel]
  performance = performance.drop("aggregation", axis = 1)

  performance = pd.melt(performance,
                        id_vars = ["variable", "region", "pattern", "station"],
                        var_name = "metric")

  metric_sel = np.logical_or(performance["variable"] == "tws",
                              performance["variable"] == "tws_alternative")
  metric_sel = np.logical_and(metric_sel,
                              performance["metric"] == "kge-{}_beta".format(variant))
  performance = performance.drop(performance.index[metric_sel])
  metric_sel = np.logical_or(performance["variable"] == "sm",
                            performance["variable"] == "sm_alternative")
  metric_sel = np.logical_and(metric_sel,
                              performance["metric"] == "kge-{}_beta".format(variant))
  performance = performance.drop(performance.index[metric_sel])

  performance["model"] = [sim.split("_")[0] for sim in performance["pattern"]]
  performance["meteo"] = [sim.split("_")[1] for sim in performance["pattern"]]
  performance["res"] = [sim.split("_")[3] for sim in performance["pattern"]]
  performance["model"] = performance["model"].replace(model_names)
  performance["meteo"] = performance["meteo"].replace(meteo_names)
  performance["res"] = performance["res"].replace(res_names)
  performance["meteores"] = ["with {} at {}".format(meteo, res) for meteo, res in zip(performance["meteo"], performance["res"])]
  performance["variable"] = performance["variable"].replace(variable_names)
  performance["region"] = performance["region"].replace(region_names)

  simulation_performance = performance.groupby(["variable", "region", "model", "metric"]).aggregate({"value": "median"})
  simulation_performance = simulation_performance.dropna()
  simulation_performance = simulation_performance.reset_index()

  additional_sel = np.array(["(additional)" in variable for variable in performance["variable"]])
  performance_base = performance.loc[~additional_sel]
  performance_additional = performance.loc[additional_sel]

  additional_sel = np.array(["(additional)" in variable for variable in simulation_performance["variable"]])
  simulation_performance_base = simulation_performance.loc[~additional_sel]
  simulation_performance_additional = simulation_performance.loc[additional_sel]

  ggplt = pn.ggplot()
  ggplt += pn.geom_boxplot(data = performance_base,
                          mapping = pn.aes(x = "metric",
                                            y = "value"),
                          fill = "lightgrey",
                          outlier_shape = "",
                          color = "black")
  ggplt += pn.geom_point(data = simulation_performance_base,
                        mapping = pn.aes(x = "metric",
                                          y = "value",
                                          color = "model",),
                        #  position = pn.position_jitter(width = 0.1),
                          size = 3,
                          alpha = 0.5)
  ggplt += pn.geom_hline(yintercept = 1.0,
                        linetype = "dashed")
  ggplt += pn.scale_x_discrete(name="Metric",
                              limits = list(metric_names.keys()),
                              breaks = list(metric_names.keys()),
                              labels = list(metric_names.values()))
  ggplt += pn.scale_y_continuous(name="",
                                breaks=[0, 1, 2])
  ggplt += pn.scale_color_manual(name = "Model",
                                  limits = [name for name in model_names.values() if name in list(performance_base["model"].unique())],
                                  values = model_colors)
  ggplt += pn.facet_grid("region~variable")
  ggplt += pn.coord_cartesian(ylim = (-1.0, 3.0))
  ggplt += pn.ggtitle("Simulation performance")
  ggplt += pn.theme(panel_background=pn.element_blank(),
                    panel_grid_major=pn.element_blank(),
                    panel_grid_minor=pn.element_blank(),
                    axis_text_x=pn.element_text(rotation=45, hjust=1),
                    legend_position="bottom",
                    # legend_direction="vertical",
                    figure_size=(9, 9))
  print(ggplt)

  plot_out = pl.Path("{}/performance-{}_base_plot.pdf".format(output_directory, variant))
  plot_out.parent.mkdir(parents=True, exist_ok=True)
  ggplt.save(plot_out)

  ggplt = pn.ggplot()
  ggplt += pn.geom_boxplot(data = performance_additional,
                          mapping = pn.aes(x = "metric",
                                            y = "value"),
                          fill = "lightgrey",
                          outlier_shape = "",
                          color = "black")
  ggplt += pn.geom_point(data = simulation_performance_additional,
                        mapping = pn.aes(x = "metric",
                                          y = "value",
                                          color = "model"),
                        #  position = pn.position_jitter(width = 0.1),
                          size = 3,
                          alpha = 0.5)
  ggplt += pn.geom_hline(yintercept = 1.0,
                        linetype = "dashed")
  ggplt += pn.scale_x_discrete(name="Metric",
                              limits = list(metric_names.keys()),
                              breaks = list(metric_names.keys()),
                              labels = list(metric_names.values()))
  ggplt += pn.scale_y_continuous(name="",
                                breaks=[0, 1, 2])
  ggplt += pn.scale_color_manual(name = "Model",
                                  limits = [name for name in model_names.values() if name in list(performance_additional["model"].unique())],
                                  values = model_colors)
  ggplt += pn.facet_grid("region~variable")
  ggplt += pn.coord_cartesian(ylim = (-1.0, 3.0))
  ggplt += pn.ggtitle("Additional simulation performance")
  ggplt += pn.theme(panel_background=pn.element_blank(),
                    panel_grid_major=pn.element_blank(),
                    panel_grid_minor=pn.element_blank(),
                    axis_text_x=pn.element_text(rotation=45, hjust=1),
                    legend_position="bottom",
                    # legend_direction="vertical",
                    figure_size=(7, 6.3))
  print(ggplt)

  plot_out = pl.Path("{}/performance-{}_additional_plot.pdf".format(output_directory, variant))
  plot_out.parent.mkdir(parents=True, exist_ok=True)
  ggplt.save(plot_out)