"""
Script for extracting URLs related to a hydrological/land surface model from an HTML page.

Author: Ehsan Modiri - UFZ
Last updated on: 2024-02-12

Usage:
- Specify the hydrological model using the 'model_name' variable.
- Optionally, provide the 'url' variable with the netCDF files' URL.
- Adjust the output file path as needed.
- Execute the script.

Output:
- Generates a list of URLs for netCDF files matching the provided model name.
- Saves the URLs to a text file at '../../data/urls_with_<model_name>[_<catchment_name>].txt'.

Libraries:
- requests
- bs4
- selenium
- pandas
"""


#%% Import libraries
import pandas as pd
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.keys import Keys


#%% Internal functions
def extract_urls(url: str, model_name: str, save_to_file: bool = True, catchment_name: str = '') -> str:
    """
    Extract URLs for netcdf files that match the given model name from an HTML page.

    Parameters:
    - url (str): URL where the netcdf files can be found.
    - model_name (str): Model name used to filter the netcdf file names.
    - save_to_file (bool): Whether to save the URLs to a file or not. Default is True.
    - catchment_name (str): Name of the catchment to append to the output file name. Default is ''.

    Returns:
    - urls (list): A list of URLs for netcdf files that match the provided model name.

    """
    # Make a GET request to the URL
    response = requests.get(url)

    # Check if the request was successful (status code 200) and model name is wflowsbm
    if model_name.lower() == "wflowsbm" and response.status_code == 200:
        # Parse the HTML content using BeautifulSoup
        soup = BeautifulSoup(response.content, 'html.parser')

        # Find all elements with names starting with the provided model name
        names = [element.text for element in soup.find_all(
            'a', href=True) if element.text.startswith(model_name)]

        # Convert the URL to a base URL by removing everything after the last forward slash
        base_url = url.rsplit('/', 2)[0] + '/' + url.rsplit('/', 2)[1] + '/'

        # Extract only the netcdf names from the full names
        netcdf_names = [name.split("/")[-1] for name in names]

        # Change "catalog" to "fileServer" in the base URL
        base_url = base_url.replace("catalog", "fileServer")

        # Combine the base URL with the netcdf names
        urls = [base_url + name for name in netcdf_names]
        print(urls)

        # Write the URLs to a file if requested
        if save_to_file:
            file_name = f"urls_with_{model_name}{'_' + catchment_name if catchment_name else ''}.txt"
            file_path = f"../../data/{file_name}"
            with open(file_path, "w") as f:
                for url in urls:
                    f.write(f"{url}\n")
    # Check if the request was successful (status code 200) and model name is not wflowsbm
    elif response.status_code == 200:
        # Set up a headless Chrome browser
        chrome_options = webdriver.ChromeOptions()
        # Run without opening a browser window
        chrome_options.add_argument('--headless')
        driver = webdriver.Chrome(
            service=ChromeService(), options=chrome_options)

        # Make a GET request to the URL
        driver.get(url)

        # Wait for some time to let the JavaScript content load
        driver.implicitly_wait(5)

        # Find all elements with names starting with the provided model name
        elements = driver.find_elements(
            By.CSS_SELECTOR, 'a[href*="{0}"]'.format(model_name))

        # Extract URLs from the elements
        urls = [element.get_attribute('href').split(
            ".nc")[0] + ".nc" for element in elements]

        # Change "catalog" to "fileServer" in the URLs
        urls = [url.replace("catalog", "fileServer") for url in urls]

        # Remove any netcdf files with no name like "/.nc"
        urls = [url for url in urls if "/.nc" not in url]

        # Write the URLs to a file if requested
        if save_to_file:
            file_name = f"urls_with_{model_name}{'_' + catchment_name if catchment_name else ''}.txt"
            file_path = f"../../data/{file_name}"
            with open(file_path, "w") as f:
                for url in urls:
                    f.write(f"{url}\n")

        # Close the browser
        driver.quit()

    else:
        print(
            f"Failed to retrieve the page. Status code: {response.status_code}")
        return []


def mhm_urls(start_year: int = 1990, end_year: int = 2021, save_to_file: bool = True) -> list:
    """
    Extract URLs for netcdf files for the mHM model.

    Parameters:
    - start_year (int): Start year for the range of netcdf files.
    - end_year (int): End year for the range of netcdf files.
    - save_to_file (bool): Whether to save the URLs to a file or not. Default is True.

    Returns:
    - urls (list): A list of URLs for netcdf files for the mHM model.

    """
    base_url = "https://minio.ufz.de/4dhydro/mHM/tier1/"
    variables = ["et", "mask", "q", "sm", "tws", "uparea"]
    forcings = ["emo", "eobs", "era5"]
    basins = ["europe", "po", "rhine", "tugela"]
    resolutions = ["0p125deg", "0p015625deg"]
    urls = []

    for variable in variables:
        for forcing in forcings:
            for basin in basins:
                for resolution in resolutions:
                    # Special case for "eobs" and "europe"
                    if forcing == "eobs" and (basin != "europe" or resolution != "0p125deg"):
                        continue
                    # "emo" has no "tugela" and is not for "europe"
                    if forcing == "emo" and (basin == "tugela" or basin == "europe"):
                        continue
                    # "era5" has no "europe" and no "0p015625deg" resolution
                    if forcing == "era5" and (basin == "europe" or resolution == "0p015625deg"):
                        continue

                    # Only one file for mask and uparea
                    if variable == "uparea" or variable == "mask":
                        file_name = f"mhm_{forcing}_{variable}_{basin}_{resolution}.nc"
                        url = f"{base_url}{file_name}"
                        urls.append(url)
                    else:
                        # Loop over years
                        for year in range(start_year, end_year + 1):
                            # Pattern is like: mhm_emo_tws_rhine_0p125deg_daily_20210101_20211231.nc
                            file_pattern = f"mhm_{forcing}_{variable}_{basin}_{resolution}_daily_"
                            file_name = f"{file_pattern}{year}0101_{year}1231.nc"
                            url = f"{base_url}{file_name}"
                            urls.append(url)

    # Write the URLs to a file if requested
    if save_to_file:
        file_path = f"../../data/urls_with_mhm.txt"
        with open(file_path, "w") as f:
            for url in urls:
                f.write(f"{url}\n")

    return urls


def read_csv(df, model_name: str, save_to_file=True):
    """
    Extracts URLs from a DataFrame based on a provided model name.

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame containing URLs in the 'Access' column.
    model_name : str
        Model name to filter URLs by.
    save_to_file : bool, optional
        Whether to save the URLs to a file or not. Default is True.

    Returns
    -------
    urls_with_model : pandas.Series
        Series containing the URLs that include the provided model name.

    """
    # Filter rows where the 'access' column contains the model_name variable
    filtered_df = df[df['Access'].str.contains(
        model_name, case=False, na=False)]

    # Extract the URLs from the filtered DataFrame
    urls_with_model = filtered_df['Access']

    if save_to_file:
        # Save the URLs to a text file without the index column and header
        urls_with_model.to_csv(
            f'../../data/urls_with_{model_name}.txt', index=False, header=False)
    else:
        print(urls_with_model)

    return urls_with_model


def read_url(model_name: str, catchment_name=''):
    """
    Retrieve the base URL for a given hydrological model and catchment.

    Parameters:
    - model_name (str): Name of the hydrological model (e.g., "clm", "geoframe", "mhm").
    - catchment_name (str, optional): Name of the catchment for specific models (e.g., "europe", "po", "rhine", "tugela").
      Defaults to ''.

    Returns:
    - str or None: The base URL for the specified hydrological model and catchment, or None if the URL is not found.

    Usage:
    - Use this function to obtain the base URL for accessing hydrological model data.

    Example:
    >>> read_url("clm", "europe")
    'https://datapub.fz-juelich.de/slts/4DHydro/data_clm_europe/'

    >>> read_url("mhm")
    'https://minio.ufz.de/4dhydro/mHM/tier1/'

    >>> read_url("pcrglobwb", "po")
    'https://geo.public.data.uu.nl/vault-4dhydro/data%5B1701782482%5D/original/pcrglobwb_erai_po_05min/'

    >>> read_url("unknown_model")
    URL for unknown_model not found.
    None
    """
    base_url_mapping = {
        "clm": "https://datapub.fz-juelich.de/slts/4DHydro/data_clm_europe/",
        "geoframe": "https://zenodo.org/records/10641131",
        "mhm": "https://minio.ufz.de/4dhydro/mHM/tier1/",
        "parflowclm": "https://datapub.fz-juelich.de/slts/4DHydro/data_parflowclm_rhine/",
        "pcrglobwb": {
            "europe": "https://geo.public.data.uu.nl/vault-4dhydro/data%5B1701782482%5D/original/pcrglobwb_erai_europe_05min/",
            "po": "https://geo.public.data.uu.nl/vault-4dhydro/data%5B1701782482%5D/original/pcrglobwb_erai_po_05min/",
            "rhine": "https://geo.public.data.uu.nl/vault-4dhydro/data%5B1701782482%5D/original/pcrglobwb_erai_rhine_05min/",
            "tugela": "https://geo.public.data.uu.nl/vault-4dhydro/data%5B1701782482%5D/original/pcrglobwb_erai_tugela_05min/",
        },
        "tetis": {
            "po": "https://zenodo.org/records/10245905",
            "tugela": "https://zenodo.org/records/10246521",
        },
        "tsmp": "https://datapub.fz-juelich.de/slts/4DHydro/data_tsmp_europe/",
        "wflowsbm": {
            "europe": "https://opendap.4tu.nl/thredds/catalog/data2/djht/bc8f15d5-5009-407d-9542-1d132c84c18c/1/catalog.html",
            "po": "https://opendap.4tu.nl/thredds/catalog/data2/djht/571fb9c3-3674-414c-8d03-f5ebbd027a64/1/catalog.html",
            "rhine": "https://opendap.4tu.nl/thredds/catalog/data2/djht/876507b7-4b4a-4e8d-a2d8-868b25ba0fda/1/catalog.html",
            "tugela": "https://opendap.4tu.nl/thredds/catalog/data2/djht/b621e626-6f6f-4e16-9747-b1c1393e2d17/1/catalog.html",
        },
    }

    try:
        url = base_url_mapping[model_name]
        if isinstance(url, dict) and catchment_name is not None:
            url = url.get(catchment_name)
        return url
    except KeyError:
        print(f"URL for {model_name} not found.")
        return None


#%% Running script
def main(model_name, catchment_name='', url=None):
    """
    Main function that extracts URLs for the provided hydrological model and saves them to a file if requested.

    Parameters:
    - model_name (str): Name of the hydrological model (options: "clm", "geoframe", "mhm", "parflowclm", "pcrglobwb", "tetis", "tsmp", "wflowsbm").
    - catchment_name (str, optional): Name of the catchment for specific models (options: "europe", "po", "rhine", "tugela"). Defaults to ''.
    - url (str, optional): URL where the netcdf files can be found. If provided, it will be used instead of fetching the URL from the model and catchment names.

    Returns:
    - None

    """
    # # Read the CSV file into a DataFrame
    # df = pd.read_csv('../../data/Products.csv')

    if model_name == 'mhm':
        mhm_urls()
        print("mHM URLs are stored!")
    else:
        # Check if the user provided the URL
        if url:
             # Extract URLs for other models
             extract_urls(
                 url,
                 model_name,
                 catchment_name=catchment_name,
                 save_to_file=True,
             )
             print(f"{model_name} URLs are stored!")
         else:
             url = read_url(model_name, catchment_name)
             # Extract URLs for other models
             extract_urls(
                 url,
                 model_name,
                 catchment_name=catchment_name,
                 save_to_file=True,
             )
             print(f"{model_name} URLs are stored!")


if __name__ == '__main__':
    # Initialize variables
    # model_name options are as follows:
    # model_name = "clm", "geofram", "mhm", "parflowclm", "pcrglowb", "tetis", "tsmp", "wflowsbm"

    # catchment_name options are as follows:
    # catchment_name = "europe", "po", "rhine", "tugela"
    main('wflowsbm', 'po')
