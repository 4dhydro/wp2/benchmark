# Hydrological Model URL Extractor

This script is written in `Python`, and users should have basic knowledge of Python to use this script.

## Overview

This script extracts URLs related to hydrological models from HTML pages. It currently supports several hydrological models and can be customized to retrieve URLs for specific models and catchments.

## Prerequisites

Make sure you have the following libraries installed:

- `requests`
- `beautifulsoup4` (Beautiful Soup - bs4)
- `selenium`
- `pandas`

You can install these libraries using the following command:

```bash
pip install requests beautifulsoup4 selenium pandas
```

Additionally, for the Selenium WebDriver, a compatible WebDriver needs to be installed. By default, the script uses the Chrome WebDriver. Make sure to install the [Chrome WebDriver](https://sites.google.com/chromium.org/driver/) and set its path in your system's `PATH`.

## Usage

1. **Set Model Name:**
   - Open the script and set the `model_name` variable to the desired hydrological model. Available options include "clm", "geoframe", "mhm", "parflowclm", "pcrglobwb", "tetis", "tsmp", and "wflowsbm".

2. **Set Catchment Name (Optional):**
   - If applicable, set the `catchment_name` variable to the desired catchment name (e.g., "europe", "po", "rhine", "tugela").

3. **Set URL (Optional):**
   - If you already have the URL where the netCDF files can be found, set the `url` variable. Otherwise, the script will attempt to generate the URL based on the model and catchment names.

4. **Adjust Output File Path:**
   - Customize the file path for the output file. By default, it saves the URLs to a text file at `../../data/urls_with_<model_name>[_<catchment_name>].txt`.

5. **Run the Script:**
   - Execute the script in your preferred Python environment.

## Output

The script produces the following output:

- A list of URLs for netCDF files that match the provided model and catchment names.
- A text file containing the URLs, saved to the specified file path.

## Examples

Change [this line](https://codebase.helmholtz.cloud/4dhydro/wp2/benchmark/-/blob/modiri_test/scripts/download/list_of_model_urls.py?ref_type=heads#L335) in the script to customize the model and catchment names. Then run the script in your preferred Python environment.

### Example 1: Extract URLs for the wflowsbm model in the "po" catchment

```python
main('wflowsbm', 'po')
```

### Example 2: Extract URLs for the parflowclm model with a specified URL

```python
main('parflowclm', url='https://datapub.fz-juelich.de/slts/4DHydro/data_parflowclm_rhine/')
```

### Example 3: Extract URLs for the clm model

```python
main('clm')
```

## Downloading NetCDF Files

To download the list of URLs, use the `wget`, `curl`, or `Invoke-WebRequest` command to download the netCDF files.

**For Unix-like systems using `wget`:**

```bash
wget -i ../../data/urls_with_<model_name>.txt -P /path/to/your/desired/folder/
```

**For Windows users using `curl`:**

```bash
curl -o /path/to/your/desired/folder/ --remote-name-all -i ../../data/urls_with_<model_name>.txt
```

**For Windows users using PowerShell and `Invoke-WebRequest`:**

```powershell
Invoke-WebRequest -Uri ((Get-Content ../../data/urls_with_<model_name>.txt) -split "`n") -OutFile '/path/to/your/desired/folder/'
```

These commands will download all the URLs listed in the `urls_with_<model_name>.txt` file to the specified folder (`/path/to/your/desired/folder/`).

Note that the `-i` argument is included for `wget` to treat the input file as a plain text file, and for the Windows commands to specify downloading of multiple URLs.

## Notes

- Ensure that you have the necessary permissions to access the URLs.
- The script uses Chrome WebDriver for Selenium. Make sure to have it installed and set in your system's `PATH`.

Feel free to customize the script or reach out to the author for any issues or additional support.