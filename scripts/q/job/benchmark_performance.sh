#!/bin/bash

#SBATCH -J geodar-0
#SBATCH --output ./log/benchmark_performance.txt
#SBATCH --error ./log/benchmark_performance.txt
#SBATCH --time 03:00:00
#SBATCH --mem 56G
#SBATCH --partition defq

echo "SBATCH job"
echo "Started $(date '+%d/%m/%Y %H:%M:%S')"
echo "Working directory $(pwd)"

module load opt/all
module load anaconda3/2023.07

conda run -n 4dHydro jupyter nbconvert --to notebook --execute benchmark_performance.ipynb --output=benchmark_performance.headless.ipynb --ExecutePreprocessor.timeout=-1

echo "Finished $(date '+%d/%m/%Y %H:%M:%S')"top