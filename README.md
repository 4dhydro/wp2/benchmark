# 4dHydro Working Package 2 Benchmark

## Description

Below is a short description of 4dHydro and Working Package 2.

### 4dHydro

The overarching objective of the Hyper-resolution Earth observations and land-surface modelling for a better understanding of the water cycle project, hereafter 4DHydro , is to foster a wide and thriving collaboration between the EO water cycle community developing novel high-resolution EO data products, and the land surface and hydrological modelling community engaged in advancing hyper-resolution modelling of the hydrological cycle at regional and continental scales. For more information see [The 4dHydro website](4dhydro.eu)

### Working Package 2

Working Package 2 has three main goals: (1) deliver a land surface model (LSM) and hydrological model (HM) community reference dataset, (2) surface model (LSM) and hydrological model (HM) dataset user manual and (3) benchmark the community reference dataset against various observations.

#### LSM/HM Community Reference Dataset

The land surface model (LSM) and hydrological model (HM) community reference dataset is a set of hydrological simulation outputs from various land-surface and hydrological models from previous studies. Therefore, this dataset represents the current state-of-the-art in hydrological modeling (i.e. before the improvements made in this project) and is used as a baseline reference for the rest of the project. Note that the hydrological simulations were not harmonized and thus consists of various different weather/climate forcings, spatial resolutions, periods and variables. However, the community reference dataset is consistently formatted according to our storage protocol (see the dataset user manual).

All community reference outputs are freely available and (in the process of being) added to the STAC-protocol. The STAC protocol is a standardized protocol for documenting spatiotemporal datasets that also includes links to download repositories for all simulations. Note that the download repositories are managed by the participants. All STAC information can be found on [the open science catalog of the 4dHydro website](4dhydro.eu/catalog).

#### LSM/HM Dataset User Manual

The land surface model (LSM) and hydrological model (HM) dataset user manual consists of documentation related to the community reference dataset. The manual consists of three main parts: (1) a description of the LSM and HM models, (2) a description of the simulations and (3) the storage protocol.

The description of the LSM and HM models details the main and secondary references to (scientific) articles describing the models, the main model features and links to the model source code and other websites. The description of the simulations details the simulation settings (e.g. period, region, resolution), a description of the main input datasets used (e.g. meteorology, soil and calibration) and the simulation output variables. The storage protocol details the consistent formatting of the community reference dataset (e.g. naming, variables and units). The user manual is (in the process of being) added to [the outputs of the 4dHydro website](4dhydro.eu/outputs).

#### LSM/HM Community Reference Benchmark

The community reference benchmark is a comparison of the community reference output against various observations using various performance metrics. Therefore, these metrics represent the current state-of-the-art performance for the models involved. Benchmarking is done in Python and Jupyter Notebooks so that the code can be easily distributed to and modified by others.

Performance is measured using the following metrics:
* normalized root mean squared error
* normalized mean error
* the Kling-Gupta efficiency (and its components)
* the non-parametric Kling-Gupta efficiency (and its components)
* the prime Kling-Gupta efficiency (and its components)

## Repository

This repository contains the Python scripts and Python Jupyther notebooks related to the benchmark. To run the Python scripts please use the environment.yml file to create a suitable Python environment using your favorite environment manager (e.g. MiniConda, AnaConda). Scripts are located in seperate directories that indicate the variable that they assess (e.g. q, et, tws, sm and combined).

### Benchmark

Daily and monthly simulated discharge, evapotranspiration and total water storage anomaly performance for each region. Simulations are compared with observed data from discharge gauges (from the GRDC, CAMELS-CH, CAMELS-GB, LamaH and various local agencies databases), evapotransiration towers (FLUXNET) and sattelite total water storage anomaly (GRACE). Four regions were assessed: Europe, Po basin, Rhine basin and Tugela basin.

The benchmark is harmonized between the different simulations, meaning that each benchmark only compares simulations and observations in the same spatial (e.g. the same basins) and temporal (e.g. the same time period) domain. Note that some simulations deviated from the spatiotemporal domain of the other simulations. For example, for GEOframe only simulations for a part of the Po basin are available. For these simulations a subsets comparison was made with other simulations that match their spatiotemporal domain.

### Create an environment for the Benchmark
The following is an easy process to create an environment for the benchmark using the provided `environment.yml` file to create a Python environment with your custom environment manager of choice such as `MiniConda` or `AnaConda`. The `environment.yml` file has all the required packages to run the benchmark. To create the environment, run the following command in your terminal:

```
cd scripts
conda env create -f environment.yml
conda activate 4dhydro
```

This command will install all dependencies needed for the benchmark in the new environment. You can change the environment's name, which is `4dhydro` in the example, to any other name of your choice.

To create an environment with a specific name, use the following command instead:

```
cd scripts
conda env create --name wp2_env --file=environment.yml
conda activate wp2_env
```

Here, the environment name is set to `wp2_env`, change the `wp2_env` to the desired name. 