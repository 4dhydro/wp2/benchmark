# Data directory
Directory containing all simulation and observation data. Simulation and observation data are described for reproducibility purposes.