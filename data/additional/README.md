# 4DHydro Project: Working Package 2 (Benchmark) - Additional Folder

Two additional sources of shape files need to be downloaded and included in the designated folders to use the code for `WP2`.
The `HydroBASINS` and `Stanford` folders contain river basin shape files and global coastline shape files, respectively.

- For the river basin shape files, the `University of Utrecht`'s written code only uses the European (EU) and African (AF) shape files at Level 5. However, all levels of basins are available for download from `HydroSHEDS`.
Please visit the `HydroBASINS` page to download these files as follows:
<https://www.hydrosheds.org/products/hydrobasins>.

    I used the following links to download the necessary shape files:

    * <https://data.hydrosheds.org/file/hydrobasins/standard/hybas_af_lev01-12_v1c.zip>
    * <https://data.hydrosheds.org/file/hydrobasins/standard/hybas_eu_lev01-12_v1c.zip>
    * <https://data.hydrosheds.org/file/hydrobasins/customized_with_lakes/hybas_lake_af_lev01-12_v1c.zip>
    * <https://data.hydrosheds.org/file/hydrobasins/customized_with_lakes/hybas_lake_eu_lev01-12_v1c.zip>
    
    **Reference:** Lehner, B., Grill G. (2013): Global river hydrography and network routing: baseline data and new approaches to study the world’s large river systems. Hydrological Processes, 27(15): 2171–2186. Data is available at <https://www.hydrosheds.org>.

- For the global coastline shape file, the `NaturalEarthData` package can be downloaded from their website at <https://www.naturalearthdata.com/downloads/>.

    The following link can be used to download the necessary files: <https://naciscdn.org/naturalearth/packages/natural_earth_vector.zip>.

    Please visit the Stanford Catalog page at <https://earthworks.stanford.edu/catalog/stanford-sg048gr3784> to download the exact utilized file.

    From there, select `Export Shapefile` to generate a download link.

    The following command can be used to download the file:

    ```
    wget https://earthworks.stanford.edu/download/file/stanford-sg048gr3784-shapefile.zip
    ```

    and to unzip it:

    ```
    unzip stanford-sg048gr3784-shapefile.zip
    ```
    **Reference:** Kelso, N.V. and Patterson, T. (2012). World Coastlines, 1:50 million (2012). Made with Natural Earth. Data is available at <https://www.naturalearthdata.com>.
