import pathlib as pl
import shutil as sh
import numpy as np
import pandas as pd
import xarray as xr

input_directory=pl.Path("./simulations")
output_directory=pl.Path("./simulations_mya")

patterns = [dir.stem for dir in input_directory.iterdir() if dir.is_dir()]

pattern = patterns[0]
for pattern in patterns:
    pattern_directory = pl.Path("{}/{}".format(input_directory, pattern))
    files = np.array([file for file in pattern_directory.rglob("*_q_*.nc") if file.is_file()])
    if len(files) == 0:
        continue
    print("Pattern: {}".format(pattern))
        
    file_fields = [file.stem.split("_") for file in files]    
    file_patterns = pd.DataFrame(data = file_fields,
                                 index = [file.stem for file in files])
    file_patterns.columns = ["model", "meteo", "variable", "region", "resolution", "timestep", "start_date", "end_date"]
    
    mya_name = "{}_{}_{}.nc".format("_".join(file_patterns[["model", "meteo", "variable", "region", "resolution", "timestep"]].iloc[0]),
                                             file_patterns["start_date"].min(),
                                             file_patterns["end_date"].max())
    mya_file = pl.Path("{}/{}/{}".format(output_directory, pattern, mya_name))
    
    if mya_file.exists():
        print("\t> Already exists, skipping...")
        continue

    dataset = xr.open_mfdataset(files,
                                parallel=True)
    
    mya_year_files = []
    
    years = np.unique(dataset["time.year"].values)
    year = years[0]
    for year in years:
        print("\tYear: {}".format(year))
        
        mya_year_name = "{}_{}0101_{}1231.nc".format("_".join(file_patterns[["model", "meteo", "variable", "region", "resolution", "timestep"]].iloc[0]),
                                                year, year)
        mya_year_file = pl.Path("{}/{}/{}/{}".format(output_directory, pattern, year, mya_year_name))
        mya_year_tmp = pl.Path("{}.tmp".format(mya_year_file))
        mya_year_files.append(mya_year_file)
        
        if mya_year_file.exists():
            print("\t\t> Already exists, skipping...")
            continue
        
        dataset_year = dataset.sel(time=slice("{}-01-01".format(year),
                                              "{}-12-31".format(year)))
        dataset_year = dataset_year.mean(dim="time")
        
        mya_year_file.parent.mkdir(parents=True, exist_ok=True)
        dataset_year.to_netcdf(mya_year_tmp)
        dataset_year.close()
        
        sh.move(mya_year_tmp, mya_year_file)
        
    dataset.close()

    dataset = xr.open_mfdataset(mya_year_files,
                                parallel=True,
                                combine="nested",
                                concat_dim = "time")
    dataset = dataset.mean(dim="time")
    
    mya_file = pl.Path("{}/{}/{}".format(output_directory, pattern, mya_name))
    mya_tmp = pl.Path("{}.tmp".format(mya_file))
    
    mya_file.parent.mkdir(parents=True, exist_ok=True)
    dataset.to_netcdf(mya_tmp)
    dataset.close()
    
    sh.move(mya_tmp, mya_file)