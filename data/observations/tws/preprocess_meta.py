import pathlib as pl
import datetime as dt
import pandas as pd
import numpy as np
import netCDF4 as nc

data_directory = pl.Path("./GRACE/data")
output_directory = pl.Path("./")

products = [dir.name for dir in data_directory.iterdir() if dir.is_dir()]

metas = []

product = products[0]
for product in products:
    product_dir = pl.Path("{}/{}".format(data_directory, product))
    
    product_fields = product.split("_")
    group = product_fields[3]
    type = product_fields[2]
    
    files = [file for file in product_dir.rglob("*.nc") if file.is_file()]
    files = np.sort(files)
    
    origins = []
    mids = []
    starts = []
    ends = []
    
    file = files[0]
    for file in files:
        dataset = nc.Dataset(file)
        times = dataset.variables["time"][:]
        times_bounds = dataset.variables["time_bounds"][:]
        dates = nc.num2date(times, dataset.variables["time"].units, dataset.variables["time"].calendar)
        dates = [dt.date(date.year, date.month, date.day) for date in dates]
        start_dates = nc.num2date(times_bounds[:, 0], dataset.variables["time"].units, dataset.variables["time"].calendar)
        start_dates = [dt.date(date.year, date.month, date.day) for date in start_dates]
        end_dates = nc.num2date(times_bounds[:, 1],
                                dataset.variables["time"].units,
                                dataset.variables["time"].calendar)
        end_dates = [dt.date(date.year, date.month, date.day) for date in end_dates]
        dataset.close()
        
        origins += [file] * len(start_dates)
        mids += dates
        starts += start_dates
        ends += end_dates
        
    mids = pd.to_datetime(mids)
    starts = pd.to_datetime(starts)
    ends = pd.to_datetime(ends)
    
    meta = {"file": origins,
            "date": mids,
            "start-date": starts,
            "end-date": ends}
    meta = pd.DataFrame(meta)
    
    meta["product"] = product
    meta["group"] = group
    meta["type"] = type
    
    metas.append(meta)
meta = pd.concat(metas)

meta.loc[meta["type"] == "L3", "end-date"] = meta.loc[meta["type"] == "L3", "end-date"] - dt.timedelta(days=1)

meta = meta[["product", "group", "type", "date", "start-date", "end-date", "file"]]
meta = meta.astype({"product": "str",
                    "group": "str",
                    "type": "str",
                    "file": "str",
                    "date": "datetime64[ns]",
                    "start-date": "datetime64[ns]",
                    "end-date": "datetime64[ns]"})

output_file = pl.Path("{}/meta.parquet".format(output_directory))
output_file.parent.mkdir(parents=True, exist_ok=True)
meta.to_parquet(output_file)

output_file = pl.Path("{}/meta.csv".format(output_directory))
output_file.parent.mkdir(parents=True, exist_ok=True)
meta.to_csv(output_file)