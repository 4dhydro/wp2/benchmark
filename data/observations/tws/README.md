# Total water storage anomaly observations
Directory containing GRACE data.

### Contents
- Gravity Recovery and Climate Experiment (GRACE) data from CSR, GFZ, JPL and CRI datasets
Landerer, F. W., and Swenson, S. C. (2012), Accuracy of scaled GRACE terrestrial water storage estimates, Water Resour. Res., 48, W04531, doi:10.1029/2011WR011453.
Data is available at grace.jpl.nasa.gov.