import pathlib as pl
import datetime as dt
import pandas as pd
import numpy as np

data_dir = pl.Path("./data")
meta_out = pl.Path("./meta.parquet")

data_files = [file for file in data_dir.rglob("*.stm") if file.is_file()]

ids = ["/".join(file.parts[1:]) for file in data_files]
depth_starts = [float(file.stem.split("_")[4]) for file in data_files]
depth_ends = [float(file.stem.split("_")[5]) for file in data_files]

meta = {"subpath": ids,
        "start-depth": depth_starts,
        "end-depth": depth_ends}
meta = pd.DataFrame(meta)

indices = []

index, row = next(meta.iterrows())
index = 5369
row = meta.loc[5369]
for index, row in meta.iterrows():
    data_file = data_files[index]
    print("Data ({}/{})".format(index, meta.index.size))
    
    header = pd.read_csv(data_file, nrows = 0, delim_whitespace = True)
    lat = float(header.columns[3])
    lon = float(header.columns[4])
    
    data = pd.read_csv(data_file, skiprows=1, header=None, delim_whitespace = True, on_bad_lines="skip")
    data = data.rename({0: "date",
                        1: "time",
                        2: "soil_moisture",
                        3: "ISMN_quality",},
                        axis=1)
    data.loc[data["ISMN_quality"] != "G", "soil_moisture"] = np.nan
    
    dates = np.array([dt.datetime.strptime(date, "%Y/%m/%d").date() for date in data["date"]])
    sm = pd.to_numeric(data["soil_moisture"], errors = "coerce").to_numpy()
    sm[sm < 0] = np.nan
    
    data_sel = np.logical_and(~np.isnan(sm), dates > dt.date(1990, 1, 1))
    dates = dates[data_sel]
    sm = sm[data_sel]
    
    if len(sm) <= 365 * 2:
        # print("Not enough sm data")
        continue
        
    meta.loc[index, "start-year"] = dates[0].year
    meta.loc[index, "end-year"] = dates[-1].year
    meta.loc[index, "average"] = np.nanmean(sm)
    meta.loc[index, "lat"] = lat
    meta.loc[index, "lon"] = lon
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))

meta = meta.loc[indices].copy()
meta["start-depth"] = pd.to_numeric(meta["start-depth"], errors = "coerce")
meta["end-depth"] = pd.to_numeric(meta["end-depth"], errors = "coerce")
    
meta = meta[["subpath", "lat", "lon", "start-year", "end-year", "start-depth", "end-depth", "average"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "start-depth", "end-depth", "average"]
meta = meta.astype({"id": "str",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "start-depth": "float32",
                    "end-depth": "float32",
                    "average": "float32"})

meta.to_parquet(meta_out)