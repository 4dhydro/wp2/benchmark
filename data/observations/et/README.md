# Evapotranspiration observations
Directory containing FLUXNET data.

### Contents
- FLUXNET 2015 dataset
Pastorello, G., Trotta, C., Canfora, E. et al. The FLUXNET2015 dataset and the ONEFlux processing pipeline for eddy covariance data. Sci Data 7, 225 (2020). https://doi.org/10.1038/s41597-020-0534-3
Data is available at fluxnet.org/data/fluxnet2015-dataset.