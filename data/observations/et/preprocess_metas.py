import pathlib as pl
import pandas as pd

meta_files = [pl.Path("./FLUXNET/meta.parquet")]
output_directory = pl.Path("./")

metas = []
for index, meta_file in enumerate(meta_files):
    meta = pd.read_parquet(meta_file)
    meta["type"] = meta_file.parent.stem
    metas.append(meta)

meta = pd.concat(metas)
meta = meta.sort_values("average", ascending = False)
meta = meta.reset_index(drop = True)

meta = meta.astype({"id": "str",
                    "type": "str"})

output_file = pl.Path("{}/meta.parquet".format(output_directory))
output_file.parent.mkdir(parents=True, exist_ok=True)
meta.to_parquet(output_file)

output_file = pl.Path("{}/meta.csv".format(output_directory))
output_file.parent.mkdir(parents=True, exist_ok=True)
meta.to_csv(output_file)