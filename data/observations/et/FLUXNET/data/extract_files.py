import pathlib as pl
import zipfile as zf
import re

raw_directory = pl.Path("./raw")
extracted_directory = pl.Path("./extracted")

zip_files = [file for file in raw_directory.rglob("*.zip") if file.is_file()]

zip_file = zip_files[0]
for zip_file in zip_files:
    zip = zf.ZipFile(zip_file)
    
    for info in zip.infolist():
        if re.match(r'.*FULLSET_DD.*\.csv$', info.filename):
            zip.extract(info,
                        extracted_directory)