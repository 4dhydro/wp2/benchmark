import pathlib as pl
import datetime as dt
import pandas as pd
import numpy as np

meta_file = pl.Path("./FLX_AA-Flx_BIF_DD_20200501.xlsx")
meta_out = pl.Path("./meta.parquet")
data_dir = pl.Path("./data/extracted")

meta = pd.read_excel(meta_file)

variable_sel = np.logical_or(meta["VARIABLE"] == "LOCATION_LAT",
                             meta["VARIABLE"] == "LOCATION_LONG")
meta_subset = meta.loc[variable_sel].copy()
meta_subset["DATAVALUE"] = pd.to_numeric(meta_subset["DATAVALUE"])
meta_group = meta_subset.groupby(["SITE_ID", "VARIABLE"]).aggregate({"DATAVALUE": "mean"})
meta_group = meta_group.reset_index()
meta_pivot = pd.pivot(meta_group, index = "SITE_ID", columns = ["VARIABLE"], values="DATAVALUE")
meta_pivot = meta_pivot.reset_index()
meta_pivot.columns.name = ""

meta_new = meta_pivot

indices = []

index, row = next(meta_new.iterrows())
for index, row in meta_new.iterrows():
    id = row["SITE_ID"]
    files = [file for file in data_dir.rglob("FLX_{}_*".format(id)) if file.is_file()]
    
    if len(files) != 1:
        print("File not found")
        continue
        
    file = files[0]
        
    data = pd.read_csv(file)
    
    dates = np.array([dt.datetime.strptime(str(date), "%Y%m%d").date() for date in data["TIMESTAMP"]])
    le = pd.to_numeric(data["LE_F_MDS"], errors = "coerce").to_numpy()
    le_qc = pd.to_numeric(data["LE_F_MDS_QC"], errors = "coerce").to_numpy()
    ta = pd.to_numeric(data["TA_F_MDS"], errors = "coerce").to_numpy()
    ta_qc = pd.to_numeric(data["TA_F_MDS_QC"], errors = "coerce").to_numpy()
    le[le < 0] = np.nan
    ta[ta < 0] = np.nan
    le[le_qc > 1] = np.nan
    ta[ta_qc > 1] = np.nan
    
    data_sel = np.logical_and(~np.isnan(le) & ~np.isnan(ta), dates > dt.date(1990, 1, 1))
    dates = dates[data_sel]
    le = le[data_sel]
    
    if len(le) <= 365 * 2:
        # print("Not enough le data")
        continue
    
    meta_new.loc[index, "start-year"] = dates[0].year
    meta_new.loc[index, "end-year"] = dates[-1].year
    meta_new.loc[index, "average"] = np.nanmean(le)
    
    indices.append(index)
    
print("Kept {} out of {} towers".format(len(indices), meta_new.index.size))

meta_new = meta_new.loc[indices].copy()

meta_new = meta_new[["SITE_ID", "LOCATION_LAT", "LOCATION_LONG", "start-year", "end-year", "average"]]
meta_new = meta_new.dropna()

meta_new.columns = ["id", "lat", "lon", "start-year", "end-year", "average"]
meta_new = meta_new.astype({"id": "str",
                            "lat": "float64",
                            "lon": "float64",
                            "start-year": "int32",
                            "end-year": "int32",
                            "average": "float32"})

meta_new.to_parquet(meta_out, index=False)