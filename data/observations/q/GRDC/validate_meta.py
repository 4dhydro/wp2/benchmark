import pathlib as pl
import pandas as pd
import numpy as np

meta_file = pl.Path("./GRDC_stations.xlsx")
meta_out = pl.Path("./meta.parquet")
data_dir = pl.Path("./data")

meta = pd.read_excel(meta_file)
meta = meta.loc[meta["d_yrs"] > 0]

indices = []

index, row = next(meta.iterrows())
for index, row in meta.iterrows():
    no = row["grdc_no"]
    file = pl.Path("{}/{}_Q_Day.Cmd.txt".format(data_dir, no))
    
    if not file.exists():
        # print("File not found: {}".format(file))
        continue
    
    avail = (row["d_end"] - row["d_start"] + 1) * 365 * (100 - row["d_miss"])
    if avail <= 365 * 2:
        # print("Not enough discharge data")
        continue
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))
  
meta = meta.loc[indices].copy()
meta["lta_discharge"] = pd.to_numeric(meta["lta_discharge"], errors = "coerce")
meta["area"] = pd.to_numeric(meta["area"], errors = "coerce")

meta = meta[["grdc_no", "lat", "long", "d_start", "d_end", "area", "lta_discharge"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "area", "average"]
meta = meta.astype({"id": "int32",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "average": "float32"})

meta.to_parquet(meta_out)