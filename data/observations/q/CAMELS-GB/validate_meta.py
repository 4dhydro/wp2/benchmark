import pathlib as pl
import pandas as pd
import numpy as np
import datetime as dt

meta_file = pl.Path("./data/CAMELS_GB_topographic_attributes.csv")
meta_out = pl.Path("./meta.parquet")
data_dir = pl.Path("./data/timeseries")

meta = pd.read_csv(meta_file, encoding = "ISO-8859-1")

indices = []

index, row = next(meta.iterrows())
for index, row in meta.iterrows():
    no = row["gauge_id"]
    file = pl.Path("{}/CAMELS_GB_hydromet_timeseries_{}_19701001-20150930.csv".format(data_dir, no))
    
    if not file.exists():
        # print("File not found: {}".format(file))
        continue
    
    data = pd.read_csv(file)    
    dates = np.array([dt.datetime.strptime(date, "%Y-%m-%d").date() for date in data["date"]])
    discharge = pd.to_numeric(data["discharge_vol"], errors = "coerce").to_numpy()
    discharge[discharge < 0] = np.nan
    
    data_sel = ~np.isnan(discharge)
    dates = dates[data_sel]
    discharge = discharge[data_sel]
    
    if len(discharge) <= 365 * 2:
        # print("Not enough discharge data")
        continue
    
    meta.loc[index, "start-year"] = dates[0].year
    meta.loc[index, "end-year"] = dates[-1].year
    meta.loc[index, "average"] = np.mean(discharge)
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))

meta = meta.loc[indices].copy()
meta["area"] = pd.to_numeric(meta["area"], errors = "coerce")

meta = meta[["gauge_id", "gauge_lat", "gauge_lon", "start-year", "end-year", "area", "average"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "area", "average"]
meta = meta.astype({"id": "int32",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "average": "float32"})

meta.to_parquet(meta_out)