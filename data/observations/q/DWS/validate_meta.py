import pathlib as pl
import pandas as pd
import numpy as np
import datetime as dt

meta_file = pl.Path("./Tugela_SITES.csv")
meta_out = pl.Path("./meta.parquet")
data_dir = pl.Path("./data")

meta = pd.read_csv(meta_file)
meta["ID"] = [id.lower() for id in meta["ID"]]

indices = []

index, row = next(meta.iterrows())
for index, row in meta.iterrows():
    no = row["ID"]
    file = pl.Path("{}/{}.csv".format(data_dir, no))
    
    if not file.exists():
        # print("File not found: {}".format(file))
        continue
    
    data = pd.read_csv(file, skiprows=4, header = None)
    data.columns = ["Date","Level (Metres)", "","Cor.Lev. (Metres)", "","Discharge (Cumecs) Final flow", ""]
    dates = np.array([dt.datetime.strptime(str(date), "%Y%m%d").date() for date in data["Date"]])
    discharge = pd.to_numeric(data["Discharge (Cumecs) Final flow"], errors = "coerce").to_numpy()
    discharge[discharge < 0] = np.nan
    
    data_sel = ~np.isnan(discharge)
    dates = dates[data_sel]
    discharge = discharge[data_sel]
    
    if len(discharge) <= 365 * 2:
        # print("Not enough discharge data")
        continue
    
    meta.loc[index, "start-year"] = dates[0].year
    meta.loc[index, "end-year"] = dates[-1].year
    meta.loc[index, "average"] = np.mean(discharge)
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))

meta = meta.loc[indices].copy()

meta = meta[["ID", "lat", "lon", "start-year", "end-year", "average"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "average"]
meta = meta.astype({"id": "str",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "average": "float32"})

meta.to_parquet(meta_out)