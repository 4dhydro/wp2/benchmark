import pathlib as pl
import pandas as pd
import numpy as np
import datetime as dt

meta_file = pl.Path("./D_gauges/1_attributes/Gauge_attributes.csv")
meta_out = pl.Path("./meta.parquet")
data_dir = pl.Path("./D_gauges/2_timeseries/daily")

meta = pd.read_csv(meta_file, encoding = "ISO-8859-1", sep = ";")

indices = []

index, row = next(meta.iterrows())
for index, row in meta.iterrows():
    no = row["ID"]
    file = pl.Path("{}/ID_{}.csv".format(data_dir, no))
    
    if not file.exists():
        # print("File not found: {}".format(file))
        continue
    
    data = pd.read_csv(file, sep = ";")
    dates = np.array([dt.date(r["YYYY"], r["MM"], r["DD"]) for _, r in data[["YYYY", "MM", "DD"]].iterrows()])
    discharge = pd.to_numeric(data["qobs"], errors = "coerce").to_numpy()
    discharge[discharge < 0] = np.nan
    
    data_sel = np.logical_and(~np.isnan(discharge), dates > dt.date(1990, 1, 1))
    dates = dates[data_sel]
    discharge = discharge[data_sel]
    
    if len(discharge) <= 365 * 2:
        # print("Not enough discharge data")
        continue
    
    meta.loc[index, "start-year"] = dates[0].year
    meta.loc[index, "end-year"] = dates[-1].year
    meta.loc[index, "average"] = np.mean(discharge)
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))

meta = meta.loc[indices].copy()
meta["area_gov"] = pd.to_numeric(meta["area_gov"], errors = "coerce")

meta = meta[["ID", "lat", "lon", "start-year", "end-year", "area_gov", "average"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "area", "average"]
meta = meta.astype({"id": "int32",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "average": "float32"})

meta.to_parquet(meta_out)