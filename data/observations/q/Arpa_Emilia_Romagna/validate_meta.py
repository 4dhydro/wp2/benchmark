import pathlib as pl
import datetime as dt
import re
import pandas as pd
import numpy as np

meta_file = pl.Path("./Arpa_Emilia_Romagna_stations.xlsx")
meta_out = pl.Path("./meta.parquet")
data_dir = pl.Path("./data")

meta = pd.read_excel(meta_file)

indices = []

index, row = next(meta.iterrows())
for index, row in meta.iterrows():
    name = row["name"]
    file = pl.Path("{}/{}.csv".format(data_dir, name))
    
    if not file.exists():
        # print("File not found: {}".format(file))
        continue
        
    data = pd.read_csv(file, skiprows=3, on_bad_lines="skip")
    data = data.iloc[:-2]
    
    dates = np.array([dt.datetime.strptime(re.sub("\\+00:00", "", date), "%Y-%m-%d %H:%M:%S").date() for date in data["Fine validità (UTC)"]])
    discharge = pd.to_numeric(data["Portata media giornaliera (M**3/S)"], errors = "coerce").to_numpy()
    discharge[discharge < 0] = np.nan
    
    data_sel = ~np.isnan(discharge)
    dates = dates[data_sel]
    discharge = discharge[data_sel]
    
    if len(discharge) <= 365 * 2:
        # print("Not enough discharge data")
        continue
    
    meta.loc[index, "start-year"] = dates[0].year
    meta.loc[index, "end-year"] = dates[-1].year
    meta.loc[index, "average"] = np.nanmean(discharge)
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))
  
meta = meta.loc[indices].copy()

meta = meta[["name", "lat", "long", "start-year", "end-year", "average"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "average"]
meta = meta.astype({"id": "str",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "average": "float32"})
    
meta.to_parquet(meta_out)