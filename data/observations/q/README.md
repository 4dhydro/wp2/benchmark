# Discharge observations 
Directory containing data from various sources.

### Contents
- GRDC dataset
The Global Runoff Data Centre, 56068 Koblenz, Germany
Data is available at www.bafg.de/GRDC.

- CAMELS-GB dataset
Coxon, G., Addor, N., Bloomfield, J. P., Freer, J., Fry, M., Hannaford, J., Howden, N. J. K., Lane, R., Lewis, M., Robinson, E. L., Wagener, T., and Woods, R.: CAMELS-GB: hydrometeorological time series and landscape attributes for 671 catchments in Great Britain, Earth Syst. Sci. Data, 12, 2459–2483, https://doi.org/10.5194/essd-12-2459-2020, 2020.

- CAMELS-CH dataset
Höge, M., Kauzlaric, M., Siber, R., Schönenberger, U., Horton, P., Schwanbeck, J., Floriancic, M. G., Viviroli, D., Wilhelm, S., Sikorska-Senoner, A. E., Addor, N., Brunner, M., Pool, S., Zappa, M., and Fenicia, F.: CAMELS-CH: hydro-meteorological time series and landscape attributes for 331 catchments in hydrologic Switzerland, Earth Syst. Sci. Data Discuss. [preprint], https://doi.org/10.5194/essd-2023-127, in review, 2023.

- LamaH dataset
Klingler, C., Schulz, K., and Herrnegger, M.: LamaH-CE: LArge-SaMple DAta for Hydrology and Environmental Sciences for Central Europe, Earth Syst. Sci. Data, 13, 4529–4565, https://doi.org/10.5194/essd-13-4529-2021, 2021.

- Department: Water and Sanitation (DWS) of the Republic of South Africa dataset
Data is available through www.dws.gov.za

- Dati Ambientali Emilia-Romagna dataset
Data is available at webbook.arpae.it

- Po dataset
Roati G. et al., AdPo 1990-2022 Dataset, 2023. To be submitted to Scientific Data.