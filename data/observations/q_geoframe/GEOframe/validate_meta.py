import warnings
import pathlib as pl
import datetime as dt
import utm
import pandas as pd
import numpy as np
import xarray as xr

meta_file = pl.Path("./features_hydrometric_gauges_VDA.csv")
mask_file = pl.Path("./geoframe_gauge_mask_po_30sec.nc")
meta_out = pl.Path("./meta.parquet")
data_file = pl.Path("./data/discharge_VDA.csv")

meta = pd.read_csv(meta_file)
meta.columns = ["gauge ID", "lon_index", "lat_index"]

dataset = xr.open_dataset(mask_file)

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=pd.errors.DtypeWarning)
    data = pd.read_csv(data_file, skiprows=2, header=1)
data = data.iloc[3:]
data.columns = [c.strip() for c in data.columns]

meta["lon"] = dataset.isel(lon = meta["lon_index"]).coords["lon"].values
meta["lat"] = dataset.isel(lat = meta["lat_index"]).coords["lat"].values

indices = []

index, row = next(meta.iterrows())
for index, row in meta.iterrows():
    id = int(row["gauge ID"])
    id_name = "value_{}".format(id)
    
    if id_name not in data.columns:
        print("ID not found: {}".format(id))
        continue
        
    dates = np.array([dt.datetime.strptime(date, "%Y-%m-%d %H:%M").date() for date in data["timestamp"]])
    discharge = pd.to_numeric(data[id_name], errors = "coerce").to_numpy(dtype = "float32")
    discharge[discharge < 0] = np.nan
    
    data_sel = np.logical_and(~np.isnan(discharge), dates > dt.date(1990, 1, 1))
    dates = dates[data_sel]
    discharge = discharge[data_sel]
    
    if len(discharge) <= 365 * 2:
        # print("Not enough discharge data")
        continue
    
    meta.loc[index, "start-year"] = dates[0].year
    meta.loc[index, "end-year"] = dates[-1].year
    meta.loc[index, "average"] = np.nanmean(discharge)
    
    indices.append(index)

print("Kept {} out of {} stations".format(len(indices), meta.index.size))

meta = meta.loc[indices].copy()

meta = meta[["gauge ID", "lat", "lon", "start-year", "end-year", "average"]]
meta = meta.dropna()

meta.columns = ["id", "lat", "lon", "start-year", "end-year", "average"]
meta = meta.astype({"id": "int32",
                    "lat": "float64",
                    "lon": "float64",
                    "start-year": "int32",
                    "end-year": "int32",
                    "average": "float32"})
    
meta.to_parquet(meta_out)